//Gets the text of the first element of a node by classname. If it doesn't exist return empty string.
function getTextByClassName(node, classname) {
	var element = node.getElementsByClassName(classname);
	if (element.length < 1) {
		return '';
	} else {
		return element[0].innerText;
	}
}

//Sends an JSON document to the API. 
function SendJSONToAPI(json) {
	var xhttp = new XMLHttpRequest();
	var ready = true;

	xhttp.onreadystatechange = function () {
		if (this.readyState == 4 && this.status == 200) {
			//success
		} else {
			ready = false;
			console.log("API not available.");
		}
	};

	xhttp.open("POST", "api.capturedata.ie", true);
	xhttp.setRequestHeader("Content-type", "application/json");

	//Only send json if service is available
	if (ready)
		xhttp.send(json);

	//Print output for debug purposes
	console.log("Output:\n " + json);
}

//Creates the JSON document from the summary page. Returns JSON document.
function createJSONDocument() {
	var flightList = new Array();

	//Loop for every flight summary
	var flightSummaryClasses = document.getElementsByClassName("flight-summary__leg-wrapper");
	for (var i = 0; i < flightSummaryClasses.length; i++) {
		var flightSummary = flightSummaryClasses[i];

		var departuresList = new Array();

		//Loop for every listed departure
		var flightSummaryDepartureClasses = flightSummary.getElementsByClassName("flight-summary__airport flight-summary--departure");
		for (var x = 0; x < flightSummaryDepartureClasses.length; x++) {
			var flightSummaryDeparture = flightSummaryDepartureClasses[x];

			departuresList.push({
				departureDate: getTextByClassName(flightSummary, "flight-summary__date"),
				departureTime: getTextByClassName(flightSummaryDeparture, "flight-summary__time hide-all show-tiny show-small"),
				departureAirportCode: getTextByClassName(flightSummaryDeparture, "flight-summary__iataCode hide-all show-tiny show-small"),
				departureCity: getTextByClassName(flightSummaryDeparture, "flight-summary__city")
			});
		}		

		var arrivalsList = new Array();

		//Loop for every listed arrival
		var flightSummaryArrivalClasses = flightSummary.getElementsByClassName("flight-summary__airport flight-summary--arrival");
		for (var x = 0; x < flightSummaryArrivalClasses.length; x++) {
			var flightSummaryArrival = flightSummaryArrivalClasses[x];

			arrivalsList.push({
				arrivalTime: getTextByClassName(flightSummaryArrival, "flight-summary__time hide-all show-tiny show-small"),
				arrivalAirportCode: getTextByClassName(flightSummaryArrival, "flight-summary__iataCode hide-all show-tiny show-small"),
				arrivalCity: getTextByClassName(flightSummaryArrival, "flight-summary__city")
			});
		}

		flightList.push({
			numberOfStops: getTextByClassName(flightSummary, "flight-summary__number-stops"),
			duration: getTextByClassName(flightSummary, "flight-summary__duration"),
			departures: departuresList,
			arrivals: arrivalsList
		});
	}

	//Return the created document JSON
	return JSON.stringify(flightList, null, 2);
}

SendJSONToAPI(createJSONDocument());