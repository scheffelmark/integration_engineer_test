This is the final commit for the integration engineer test. 

To create script 1 I used www.cheaptickets.nl/en. For the second script I added www.ryanair.com.
I tested the script in Chrome, Internet Explorer 11 and Microsoft Edge.

I made the following assumptions while doing this test:
    1. The flight data can be sent as a JSON file.
    2. It isn't a problem that the information presented on the page is subject to localisation. 