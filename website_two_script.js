//Holds the class names where the values are located. 
class ClassNames {
	constructor(flightNumber, flightSummaryClasses, flightSummaryDepartureClasses, departureDate, departureTime, departureAirportCode,
		departureCity, numberOfStops, duration, flightSummaryArrivalClasses,
		arrivalTime, arrivalAirportCode, arrivalCity) {
		this.flightNumber = flightNumber;
		this.flightSummaryClasses = flightSummaryClasses;
		this.flightSummaryDepartureClasses = flightSummaryDepartureClasses;
		this.departureDate = departureDate;
		this.departureTime = departureTime;
		this.departureAirportCode = departureAirportCode;
		this.departureCity = departureCity;
		this.numberOfStops = numberOfStops;
		this.duration = duration;
		this.flightSummaryArrivalClasses = flightSummaryArrivalClasses;
		this.arrivalTime = arrivalTime;
		this.arrivalAirportCode = arrivalAirportCode;
		this.arrivalCity = arrivalCity;
	}
}

//Creates the variable class names for different sites. Returns values based on the site name, if not implemented returns empty object. 
function getClassNames(url) {
	if (url.includes("cheaptickets")) {
		return new ClassNames("",
			"flight-summary__leg-wrapper",
			"flight-summary__airport flight-summary--departure",
			"flight-summary__date",
			"flight-summary__time hide-all show-tiny show-small",
			"flight-summary__iataCode hide-all show-tiny show-small",
			"flight-summary__city",
			"flight-summary__number-stops",
			"flight-summary__duration",
			"flight-summary__airport flight-summary--arrival",
			"flight-summary__time hide-all show-tiny show-small",
			"flight-summary__iataCode hide-all show-tiny show-small",
			"flight-summary__city"
		);
	} else if (url.includes("ryanair")) {
		return new ClassNames("flight-number",
			"flights-table__flight",
			"flight-header flight-header--selected",
			"hide-mobile flight-header__details-time",
			"start-time",
			"",
			"cities__departure",
			"direct",
			"flight-header__connection-time-label",
			"flight-header flight-header--selected",
			"end-time",
			"",
			"cities__destination"
		);
	} else {
		return new ClassNames();
	}
}

//Gets the text of the first element of a node by classname. If it doesn't exist return empty string.
function getTextByClassName(node, classname) {
	var element = node.getElementsByClassName(classname);
	if (element.length < 1) {
		return '';
	} else {
		return element[0].innerText;
	}
}

//Sends an JSON document to the API. 
function SendJSONToAPI(json) {
	var xhttp = new XMLHttpRequest();
	var ready = true;

	xhttp.onreadystatechange = function () {
		if (this.readyState == 4 && this.status == 200) {
			//success
		} else {
			ready = false;
			console.log("API not available.");
		}
	};

	xhttp.open("POST", "api.capturedata.ie", true);
	xhttp.setRequestHeader("Content-type", "application/json");

	//Only send json if service is available
	if (ready)
		xhttp.send(json);

	//Print output for debug purposes
	console.log("Output:\n " + json);
}

//Creates the JSON document from the summary page. Uses ClassNames object to find values. Returns JSON document.
function createJSONDocument(classnames) {
	//Defense for invalid input.
	if (!classnames instanceof ClassNames) {
		console.log("Input not supported");
		return '';
	}

	var flightList = new Array();

	//Loop for every flight summary
	var flightSummaryClasses = document.getElementsByClassName(classnames.flightSummaryClasses);
	for (var i = 0; i < flightSummaryClasses.length; i++) {
		var flightSummary = flightSummaryClasses[i];

		var departuresList = new Array();

		//Loop for every listed departure
		var flightSummaryDepartureClasses = flightSummary.getElementsByClassName(classnames.flightSummaryDepartureClasses);
		for (var x = 0; x < flightSummaryDepartureClasses.length; x++) {
			var flightSummaryDeparture = flightSummaryDepartureClasses[x];

			departuresList.push({
				departureDate: getTextByClassName(flightSummary, classnames.departureDate),
				departureTime: getTextByClassName(flightSummaryDeparture, classnames.departureTime),
				departureAirportCode: getTextByClassName(flightSummaryDeparture, classnames.departureAirportCode),
				departureCity: getTextByClassName(flightSummaryDeparture, classnames.departureCity)
			});
		}

		var arrivalsList = new Array();

		//Loop for every listed arrival
		var flightSummaryArrivalClasses = flightSummary.getElementsByClassName(classnames.flightSummaryArrivalClasses);
		for (var x = 0; x < flightSummaryArrivalClasses.length; x++) {
			var flightSummaryArrival = flightSummaryArrivalClasses[x];

			arrivalsList.push({
				arrivalTime: getTextByClassName(flightSummaryArrival, classnames.arrivalTime),
				arrivalAirportCode: getTextByClassName(flightSummaryArrival, classnames.arrivalAirportCode),
				arrivalCity: getTextByClassName(flightSummaryArrival, classnames.arrivalCity)
			});
		}

		flightList.push({
			flightnumber: getTextByClassName(flightSummary, classnames.flightNumber),
			numberOfStops: getTextByClassName(flightSummary, classnames.numberOfStops),
			duration: getTextByClassName(flightSummary, classnames.duration),
			departures: departuresList,
			arrivals: arrivalsList
		});
	}	

	//Return the created document JSON
	return JSON.stringify(flightList, null, 2);
}

SendJSONToAPI(createJSONDocument(getClassNames(window.location.hostname)));